# This script makes concurrent WMS requests to a Geoserver layer to evaluate performance.
# The runningAverage variable in the output is the most useful.

# Import
import argparse
import time
import random
import grequests
from PIL import Image
from StringIO import StringIO


def get_image_dates(erasToUse):
    # Make a list of date strings
    # Todo - Extract from getcapabilities
    imageDates = ["1987-12","1988-03","1988-06","1988-09","1988-12","1989-03","1989-06","1989-09","1989-12","1990-03","1990-06","1990-09","1990-12","1991-03","1991-06","1991-09","1991-12","1992-03","1992-06","1992-09","1992-12","1993-03","1993-06","1993-09","1993-12","1994-03","1994-06","1994-09","1994-12","1995-03","1995-06","1995-09","1995-12","1996-03","1996-06","1996-09","1996-12","1997-03","1997-06","1997-09","1997-12","1998-03","1998-06","1998-09","1998-12","1999-03","1999-06","1999-09","1999-12","2000-03","2000-06","2000-09","2000-12","2001-03","2001-06","2001-09","2001-12","2002-03","2002-06","2002-09","2002-12","2003-03","2003-06","2003-09","2003-12","2004-03","2004-06","2004-09","2004-12","2005-03","2005-06","2005-09","2005-12","2006-03","2006-06","2006-09","2006-12","2007-03","2007-06","2007-09","2007-12","2008-03","2008-06","2008-09","2008-12","2009-03","2009-06","2009-09","2009-12","2010-03","2010-06","2010-09","2010-12","2011-03","2011-06","2011-09","2011-12","2012-03","2012-06","2012-09","2012-12","2013-03","2013-06","2013-09","2013-12","2014-03","2014-06","2014-09","2014-12","2015-03","2015-06","2015-09","2015-12","2016-03","2016-06","2016-09","2016-12","2017-03","2017-06","2017-09","2017-12","2018-03","2018-06","2018-09","2018-12","2019-03"]

    # Subset for testing a smaller subset
    erasToUse = min(erasToUse,len(imageDates))
    print "Using %s eras" % str(erasToUse)
    imageDates = imageDates[0:erasToUse]

    return imageDates


def exception_handler(request, exception):
    print request


def gcAreaMean(wms_server, pointx, pointy, delta, layer, image_dates):
    currentTime = time.time()
    meanBareGround = []
    # Setup the WMS URL for download
    bbox = str(pointx-delta) + "," + str(pointy-delta) + "," + str(pointx+delta) + ","+str(pointy+delta)

    wmsURL = wms_server + "/aus/wms?service=WMS&version=1.1.0&request=GetMap&layers=aus:"+layer+"&styles=&width=64&height=64&srs=EPSG:4326&format=image/tiff&bbox="+bbox+"&time="

    # Create a set of unsent server requests:
    rs = (grequests.get(wmsURL + date) for date in image_dates)
    # Query the server
    timeSeriesWMS = grequests.map(rs, exception_handler=exception_handler)
    # Unpack the data
    for fcImage in timeSeriesWMS:
      try:
        bareGround = list(Image.open(StringIO(fcImage.content)).split()[0].getdata())
        bareGround = [bg for bg in bareGround if 100 < bg < 200]
        meanBareGround.append(float(sum(bareGround)) / max(len(bareGround), 1))
      except Exception as e:
          if fcImage is None:
              print e
              continue

          # This exception occurs if the request is outside Australia's borders.
          # It only happens with Geoserver 2.15.1, it did not happen with Geoserver 2.9.3
          if 'CannotCropException' not in fcImage.content:
              print fcImage.content

    processTime = (time.time() - currentTime)
    meanBareGround = float(sum(meanBareGround)) / max(len(meanBareGround), 1)
    return (processTime, meanBareGround)


def run(layer, eras, wms_server):
    # Variables for holding the running mean
    trialCount=0
    trialSum=0

    print 'Using {0}'.format(wms_server)

    image_dates = get_image_dates(eras)
    # Main Program
    print "longitude latitude deltaDegrees bareMean timeSeconds,runningAverage"
    # Run 100 trials
    for i in range(200):
        xPoint = random.uniform(115, 150)
        yPoint = random.uniform(-35, -15)
        delta = random.uniform(0, 2)
        (requestTime,bareGround) =  gcAreaMean(wms_server, xPoint,yPoint,delta, layer, image_dates)
        trialSum += requestTime
        trialCount += 1
        print xPoint, yPoint, delta, bareGround,requestTime, trialSum/trialCount


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--layer', dest='layer')
    parser.add_argument(
        '--eras', dest='eras', type=int, help='The number of eras to make concurrent requests for')
    parser.add_argument(
        '--wms-server', dest='wms_server')

    args = parser.parse_args()

    run(args.layer, args.eras, args.wms_server)

if __name__ == '__main__':
    main()

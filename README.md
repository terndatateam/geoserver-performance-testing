# Geoserver Performance Testing

This repository contains a program to submit concurrent WMS requests to a Geoserver layer. It was written by Peter Scarth from TERN Landscapes. The script is largely unmodified so that the same behaviour that Peter experienced can be reproduced.

## Prerequisites

* Python 2.7

## Getting started

```
# Set up the virtual environment
python2.7 -m virtualenv venv
. venv/bin/activate
python -m pip install -r requirements.txt

# Make 70 concurrent requests for each row of output
python geoserver_testing --layer=fractional_cover --eras=70 -wms-server=https://geoserver.tern.org.au/geoserver
```

## Interpreting the output

Each row in the output represents the resposne time for X number of requests,
where X is the number of eras that have been configured.

The running average column is the most useful. It is recomputed after each set of requests
is returned.

```
$ python geoserver_testing.py --layer=fractional_cover --eras=70
Using 70 eras
longitude latitude deltaDegrees bareMean timeSeconds,runningAverage
142.675460605 -24.6203020343 1.10018520016 146.580608666 3.86562490463 3.86562490463
141.54183781 -33.0108311949 1.15652696174 142.998153247 1.99883103371 2.93222796917
140.261672672 -32.0566856224 1.90764577218 151.647477168 5.10980176926 3.65808590253
132.182412719 -22.4962407638 1.34541222311 159.200770855 1.86856913567 3.21070671082
139.295409966 -26.9758848608 0.287752658221 152.179968043 5.11706900597 3.59197916985
120.166143517 -22.5778569333 1.48026004432 151.758737056 2.34190011024 3.38363265991
134.214769378 -15.2866440454 1.86336248059 116.223797695 1.57568907738 3.12535500526
143.860257982 -19.5520857054 1.80149061963 127.678880353 2.60152316093 3.05987602472
123.206109856 -23.4856165751 1.70769788753 170.175480836 0.916838884354 2.8217607869
```
